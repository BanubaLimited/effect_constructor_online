module.exports = {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|ts|svelte)"],
  addons: ["@storybook/addon-essentials", "@storybook/addon-svelte-csf"],
  webpackFinal: (config) => {
    config.resolve.alias["./reset-css"] = require.resolve("reset-css")
    return config
  },
  svelteOptions: {
    preprocess: require("../svelte.config.cjs").preprocess,
  },
}

import Layout from "../src/routes/__layout.svelte" // global styles

export const decorators = [() => Layout]

export const parameters = {
  backgrounds: {
    default: "default",
    values: [
      {
        name: "lighter",
        value: "#2f3147",
      },
      {
        name: "default",
        value: "#212337",
      },
      {
        name: "darker",
        value: "#161724",
      },
    ],
  },
}

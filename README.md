# Banuba Web Studio

### Staging

[![Netlify Status](https://api.netlify.com/api/v1/badges/3f0a4fee-ed35-42ef-91ca-738c44d95a6e/deploy-status)](https://app.netlify.com/sites/studiobanubacom/deploys)

https://studiobanubacom.netlify.app/

## Developing

```bash
# installing dependencies
npm install

# You need git lfs for static files
# in your root directory make
git lfs install
git lfs pull
```

```bash
# Start the server
npm run dev
# Or start the server and open the app in a new browser tab
npm run dev --open
```

## Building

With current configuration `npm run build` will prerender entire site as a collection of static files using [@sveltejs/adapter-static](https://github.com/sveltejs/kit/tree/master/packages/adapter-static)

Svelte apps are built with _adapters_, which optimise your project for deployment to different environments.

To use a different adapter, add it to the `devDependencies` in `package.json` making sure to specify the version as `next` and update your `svelte.config.cjs` to [specify your chosen adapter](https://kit.svelte.dev/docs#configuration-adapter). The following official adapters are available:

- [@sveltejs/adapter-node](https://github.com/sveltejs/kit/tree/master/packages/adapter-node)
- [@sveltejs/adapter-netlify](https://github.com/sveltejs/kit/tree/master/packages/adapter-netlify)
- [@sveltejs/adapter-vercel](https://github.com/sveltejs/kit/tree/master/packages/adapter-vercel)
- ...more soon

[See the adapter documentation for more detail](https://kit.svelte.dev/docs#adapters)

## Linter

```bash
# Linter check
npm run lint

# Format files
npm run format
```

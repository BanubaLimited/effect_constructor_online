module.exports = {
  plugins: [
    require("postcss-preset-env"),
    require("postcss-nesting"),
    require("postcss-aspect-ratio-polyfill"),
  ],
}

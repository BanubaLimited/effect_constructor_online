import type { AmplitudeClient } from "amplitude-js"
import { analyticsEvents } from "./analyticsEvents"

type AmplitudeEventProps = Record<string | number, number | string | boolean>

const amplitude: Promise<AmplitudeClient> = import("amplitude-js").then(
  ({ default: amplitude }) => {
    amplitude.options.apiEndpoint = import.meta.env.VITE_AMPLITUDE_ENDPOINT
    amplitude.options.disableCookies = true
    amplitude.options.saveEvents = false

    const client = amplitude.getInstance("banuba")

    client.init(import.meta.env.VITE_ANALYTICS_TOKEN, "", {
      apiEndpoint: import.meta.env.VITE_AMPLITUDE_ENDPOINT,
      cookieForceUpgrade: true,
      saveEvents: false,
      disableCookies: true,
    })

    return client
  }
)

export const logEvent = (eventName: string, props?: AmplitudeEventProps) => {
  window.dataLayer?.push({ event: eventName })
  amplitude.then((client) => client.logEvent(eventName, props ?? {}))
}

export const googleAnalytics = {
  objects: {
    addNewObject: () => logEvent(analyticsEvents.ADD_NEW_OBJECT),
  },

  topBar: {
    newEffect: () => logEvent(analyticsEvents.NEW),
    importEffect: () => logEvent(analyticsEvents.IMPORT),
    feedbackSent: () => logEvent(analyticsEvents.FEEDBACK_SENT),
    exportEffect: () => logEvent(analyticsEvents.EXPORT),
  },

  editor: {
    scriptingOpen: () => logEvent(analyticsEvents.SCRIPTING_OPEN),
    scriptingSave: () => logEvent(analyticsEvents.SCRIPTING_SAVE),
  },
}

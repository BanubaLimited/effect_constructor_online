export enum analyticsEvents {
  EXPORT = "export_button_click",
  FEEDBACK_SENT = "feedback_send",
  ADD_NEW_OBJECT = "add_new_object",
  IMPORT = "import",
  NEW = "new_button_click",
  SCRIPTING_OPEN = "scripting_open_click",
  SCRIPTING_SAVE = "scripting_save",
}

<script lang="ts">
  export let secondary = false
  export let tertiary = false
  export let disabled = false
</script>

<button class={$$props.class} class:secondary class:tertiary {disabled} on:click>
  <slot />
</button>

<style lang="postcss">
  button {
    display: flex;
    align-items: center;
    padding: 0.25rem;
    background: transparent;
    border: none;
    outline: none;
    cursor: pointer;
    color: var(--text-color);
    font-size: var(--font-size-m);
    font-weight: bold;
    transition: color 0.25s ease-out;
    justify-content: center;

    &:hover {
      transition: color 0.125s ease-in;
    }

    & > :global(svg + *) {
      margin-left: 0.3125rem;
    }
  }

  .secondary {
    color: var(--text-color-secondary);

    &:hover {
      color: var(--text-color);
    }
  }

  .tertiary {
    color: var(--text-color-tertiary);

    &:hover {
      color: var(--text-color-secondary);
    }
  }

  button:disabled {
    opacity: 0.3;
  }
</style>

export class WebStudioException extends Error {
  constructor(msg: string) {
    super(msg)
    this.name = "WebStudioException"
  }
}

export class UnsupportedFormatException extends WebStudioException {
  constructor(msg: string) {
    super(msg)
    this.name = "UnsupportedFormatException"
  }
}

export class UnsupportedGLTFobjectException extends WebStudioException {
  constructor(msg: string) {
    super(msg)
    this.name = "UnsupportedGLTFobjectException"
  }
}

export class UnsupportedEffectException extends WebStudioException {
  constructor(msd: string) {
    super(msd)
    this.name = "UnsupportedEffectException"
  }
}

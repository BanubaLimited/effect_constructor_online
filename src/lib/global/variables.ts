export const variables = {
  emailJsClientServiceId: import.meta.env.VITE_EMAILJS_CLIENT_SERVICE_ID,
  emailJsTemplateId: import.meta.env.VITE_EMAILJS_TEMPLATE_ID,
  ga: {
    globalSiteGtag: import.meta.env.VITE_GLOBAL_SITE_GTAG,
    gtag: import.meta.env.VITE_GTAG,
  },
  stg: import.meta.env.MODE === "staging",
}

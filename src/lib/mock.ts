export const materialsName = [{ title: "Name materials" }]

export const blendOptions = [
  { title: "Off" },
  { title: "Premul alpha aaaaa" },
  { title: "Screen" },
  { title: "Add" },
  { title: "Multiply" },
  { title: "Coverage1" },
]

export const shaderOptions = [{ title: "PBR" }, { title: "CUT" }, { title: "Billboard" }]

export const blendMode = [
  { title: "Normal", value: "normal" },
  { title: "Multiply", value: "multiply" },
  { title: "Screen", value: "screen" },
  { title: "Overlay", value: "overlay" },
  { title: "Soft Light", value: "soft_light" },
  { title: "Hard Light", value: "hard_light" },
]

export const pressets = [
  { title: "Off" },
  { title: "Cartoon" },
  { title: "Dither" },
  { title: "Grandma_TV_TV" },
  { title: "Prettyboi" },
  { title: "Rain" },
]

export const lutPressets = [
  { title: "Byers", url: "/pressets/luts/Byers.png" },
  { title: "Fuji", url: "/pressets/luts/Fuji.png" },
  { title: "Full Moon", url: "/pressets/luts/Full_Moon.png" },
  { title: "Gray", url: "/pressets/luts/Gray.png" },
  { title: "LUX", url: "/pressets/luts/LUX.png" },
  { title: "Nevada", url: "/pressets/luts/Nevada.png" },
  { title: "New Zealand", url: "/pressets/luts/New_Zealand.png" },
  { title: "Olive", url: "/pressets/luts/Olive.png" },
  { title: "Peach", url: "/pressets/luts/Peach.png" },
  { title: "Pink", url: "/pressets/luts/Pink.png" },
  { title: "Santa", url: "/pressets/luts/Santa.png" },
]

export const backgroundPressets = [
  { title: "80s", url: "/pressets/background/80s.mp4" },
  { title: "Beach", url: "/pressets/background/Beach.mp4" },
  { title: "Car", url: "/pressets/background/Car.png" },
  { title: "Cartoonroom", url: "/pressets/background/Cartoonroom.png" },
]

export const lipsPressets = [
  {
    title: "Coral",
    opacity: 0.6,
    shine: 1.02,
    brightness: 0.85,
    saturation: 0.93,
    color: "#FF7F50",
  },
  { title: "Red", opacity: 0.8, shine: 1.02, brightness: 0.7, saturation: 1.0, color: "#ff0404" },
  {
    title: "Violet",
    opacity: 0.8,
    shine: 1.03,
    brightness: 0.7,
    saturation: 1.02,
    color: "#ff04f3",
  },
  { title: "Pink", opacity: 1.0, shine: 1.0, brightness: 0.86, saturation: 1.5, color: "#ffb7b7" },
]

export const hairPressets = [
  { title: "Coral", opcity: null, color: "#dd5959" },
  { title: "Gray", opcity: null, color: "#727272" },
  { title: "Ariel", opcity: 0.9, color: "#e72b2b" },
  { title: "Brown", opcity: 0.9, color: "#76420f" },
  { title: "Joker", opcity: null, color: "#22880b" },
  { title: "Lila", opcity: 0.9, color: "#a81dce" },
]

export const eyesPressets = [
  { title: "Red", opacity: 0.9, color: "#ff0404" },
  { title: "Green", opacity: 0.8, color: "#2c7a17" },
  { title: "Sky", opacity: 0.7, color: "#1dafce" },
  { title: "Witch", opacity: 0.75, color: "#c70ebe" },
  { title: "Emerald", opacity: 0.7, color: "#42c200" },
  { title: "Ocean", opacity: 0.65, color: "#161e75" },
  { title: "Gray", opacity: 0.9, color: "#848484" },
]

export const shadersPressets = [
  { title: "Default", url: "/pressets/shaders/default_shaders.zip" },
  { title: "Cartoon", url: "/pressets/shaders/shadersCartoon.zip" },
  { title: "Dice_me_up", url: "/pressets/shaders/shadersDice_me_up.zip" },
  { title: "Dither", url: "/pressets/shaders/shadersDither.zip" },
  { title: "Game_boy_camera", url: "/pressets/shaders/shadersGame_boy_camera.zip" },
  { title: "Newsprint", url: "/pressets/shaders/shadersNewsprint.zip" },
]

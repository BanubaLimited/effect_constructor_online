import { derived } from "svelte/store"
import { effect, studio } from "."
import { ColorCorrection } from "./objects/color-correction"

export default derived<[typeof studio, typeof effect], ColorCorrection>(
  [studio, effect],
  ([{ player }, _], set) => set(new ColorCorrection(player))
)

import { derived, writable } from "svelte/store"
import { studio } from "."
import { effect } from "./effect"
import { CodeEditor } from "./objects/code-editor"

const re = /\[.*\|ERROR\]\[java script\](.*)/

export const errorLog = writable<Set<string>>(new Set())

export const editor = derived([studio, effect], ([{ player }, $effect], set) => {
  if ($effect) {
    set(new CodeEditor(player))

    player.addEventListener("log", (e: CustomEvent) => {
      const err = e.detail
      if (re.test(err)) {
        const idx = err.indexOf("exception")
        errorLog.update((state) => new Set([...state, err.substr(idx)]))
      }
    })
  } else {
    set(null)
    errorLog.set(new Set())
  }
})

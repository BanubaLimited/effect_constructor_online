import _ from "lodash"
import studio from "./studio"
import type { Beautification, Environment, ColorCorrection } from "./objects"
import type { BanubaSDK } from "@banuba/webar"
import { Player } from "@banuba/webar"
import { nanoid } from "nanoid"
import { derived, writable } from "svelte/store"
import { Effect } from "@banuba/webar"
import { UnsupportedFormatException } from "$lib/global/exceptions"
import { Entity } from "./objects"
import { Lightning } from "./objects/lightning"
import { effectLoading, EffectManager } from "./objects/effect-manager"
import { addScriptField, differenceDump, readDirRecursive } from "./utils/fs"
import { download } from "./utils/file-reader"
import { googleAnalytics } from "$lib/analytics"
import { generateEffectFromGltf, deleteConverter } from "./objects/converter/converter"

const { mapKeys } = _

export { Entity }

const DEFAULT_EFFECT_NAME = "New_Effect"

export const effectName = writable<string>(null)

export const object = writable<Entity | Beautification | Environment | ColorCorrection | Lightning>(
  null
)

export const makeup = writable<boolean | null>(null)
export const isBlankEffect = writable<boolean>(true)

export const effect = derived<typeof studio, BanubaSDK.Effect>(studio, ({ player }, set) => {
  const effectManager = new EffectManager(player)

  player.addEventListener(Player.EFFECT_ACTIVATED_EVENT, async (e) => {
    if (e?.detail?.url()) {
      set(e.detail)
    } else {
      deleteConverter()
      set(null)
      await effectManager.createBlankEffect()
      makeup.set(true)
      effectName.set(DEFAULT_EFFECT_NAME)
    }
  })
})

const isLightningActive = writable(false)

export const lightningTextureName = writable<string>("")

export const lightning = derived<
  [typeof studio, typeof effect, typeof isLightningActive, typeof isBlankEffect],
  Lightning
>(
  [studio, effect, isLightningActive, isBlankEffect],
  ([{ player }, $effect, $lightning, $blank], set) => {
    if ($blank) return set(null)
    if (player && $effect && !$lightning) {
      $lightning = true
      set(new Lightning(player))
    } else {
      $lightning = false
      set(null)
    }
  }
)

export default derived([studio, effect, effectName], ([{ player }, $effect]) => ({
  async new() {
    await player.clearEffect()
    object.set(null)
    makeup.set(null)
    isBlankEffect.set(true)
    lightningTextureName.set("")
    googleAnalytics.topBar.newEffect()
    deleteConverter()
  },

  async load(value: Blob | string, isGltf?: boolean) {
    isBlankEffect.set(false)

    try {
      if (!isGltf) deleteConverter()
      makeup.set(false)
      googleAnalytics.topBar.importEffect()
      object.set(null)

      effectLoading.set(true)
      const source = await Effect.preload(value as any) // FIXME: Effect.preload should also accept Blob
      const _effect = await player.applyEffect(source)

      effectLoading.set(false)

      const effectManager = new EffectManager(player, isGltf)

      effectManager.setEffectVolume(1)
      effectName.set(_effect.url()) // FIXME: this is actually effectUrl, not the effectName

      effectManager.clearScripting(_effect)

      makeup.set(await effectManager.setMakeup())
    } catch (e) {
      console.warn(e)
      await player.clearEffect()
      throw new UnsupportedFormatException(
        "The file appears to be unsupported or damaged. Blank effect returned"
      )
    }
  },

  async import(gltf: File, physics: boolean) {
    await this.load(await generateEffectFromGltf(gltf, physics), true)
  },

  export() {
    if (!$effect) return

    const fs = player["_sdk"].FS
    const dumpPath = `/tmp/${nanoid()}`
    const url = $effect.url()

    $effect.dumpFs(dumpPath)
    differenceDump(url, dumpPath, fs)
    addScriptField(fs, dumpPath)

    const files = readDirRecursive(fs, dumpPath)
    const readyFiles = mapKeys(files, (_, key) => key.replace(dumpPath, DEFAULT_EFFECT_NAME))

    download(readyFiles, `${DEFAULT_EFFECT_NAME}.zip`)
    googleAnalytics.topBar.exportEffect()
  },
}))

export const entities = derived([studio, effect], ([{ player }, $effect], set) => {
  if (!$effect) return set(null)

  const root = new Entity(player, $effect.scene().getRoot())
  const entities = root.children

  ;(async () => {
    // Scene entities are prepared asynchronously :(
    while (!entities.some((e) => e.active)) await new Promise((r) => setTimeout(r, 300))
    set(entities)
  })()

  const subscribeForRemoval = (entity: Entity) => {
    entity.once("remove", () => {
      const root = new Entity(player, $effect.scene().getRoot())

      // We need to resubscribe to event when getting new tree
      subscribeForRemoval(root)

      const entities = root.children
      set(entities)
    })
  }

  subscribeForRemoval(root)
})

export const beautification = derived([studio, effect], ([{ sdk }, $effect]) => null)

export const environment = derived([studio, effect], ([{ sdk }, $effect]) => null)

export const color = derived([studio, effect], ([{ sdk }, $effect]) => null)

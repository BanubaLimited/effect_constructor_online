import emailjs from "emailjs-com"
import { derived, writable } from "svelte/store"
import { browser } from "$app/env"

const wasInit = writable<boolean>(false)

export const email = derived(wasInit, ($wasInit, set) => {
  if ($wasInit || !browser) return
  emailjs.init(import.meta.env.VITE_EMAILJS_USER_ID as string)
  set(emailjs)
  wasInit.set(true)
})

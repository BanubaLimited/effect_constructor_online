import { derived, writable } from "svelte/store"
import { effect, studio } from "."
import { makeup } from "./effect"
import { Environment, ENV_FEATURES } from "./objects/environment"

export const selectedEnvFeature = writable<keyof typeof ENV_FEATURES | null>()

export default derived<[typeof studio, typeof effect, typeof makeup], Environment>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) {
      set(new Environment(player))
      selectedEnvFeature.set(null)
    }
  }
)

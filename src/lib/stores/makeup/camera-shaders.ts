import { derived, writable } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { Shader } from "../objects/makeup/camera-shaders"

export default derived<[typeof studio, typeof effect, typeof makeup], Shader>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(Shader.create(player))
  }
)

interface Option {
  id?: number
  title: string
  url?: string
  value?: string | number
}

export const selectedShader = writable<Option | null>()

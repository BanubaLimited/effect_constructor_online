import { derived } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { EyesColor } from "../objects/makeup/eyes-color"

export default derived<[typeof studio, typeof effect, typeof makeup], EyesColor>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new EyesColor(player))
  }
)

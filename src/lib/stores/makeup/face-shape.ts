import { derived } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { FaceShape } from "../objects/makeup/face-shape"

export default derived<[typeof studio, typeof effect, typeof makeup], FaceShape>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new FaceShape(player))
  }
)

import { derived } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { HairColor } from "../objects/makeup/hair-color"

export default derived<[typeof studio, typeof effect, typeof makeup], HairColor>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new HairColor(player))
  }
)

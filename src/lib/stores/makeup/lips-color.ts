import { derived } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { LipsColor } from "../objects/makeup/lips-color"

export default derived<[typeof studio, typeof effect, typeof makeup], LipsColor>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new LipsColor(player))
  }
)

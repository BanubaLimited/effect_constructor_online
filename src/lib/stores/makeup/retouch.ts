import { derived } from "svelte/store"
import studio from "../studio"
import { effect, makeup } from "../effect"
import { Retouch } from "../objects/makeup/retouch"

export default derived<[typeof studio, typeof effect, typeof makeup], Retouch>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new Retouch(player))
  }
)

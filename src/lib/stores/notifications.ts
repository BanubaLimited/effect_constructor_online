import { nanoid } from "nanoid"
import { writable } from "svelte/store"

const TIMEOUT = 7000

const { subscribe, update } = writable([])

const send = (message: string, type = "info") =>
  update((notifications) => {
    const notification = {
      id: nanoid(),
      type,
      message,
    }

    setTimeout(() => {
      update((notifications) => notifications.filter((n) => n.id !== notification.id))
    }, TIMEOUT)

    return [...notifications, notification]
  })

const info = (message: string) => send(message, "info")
const warn = (message: string) => send(message, "warn")
const error = (message: string) => send(message, "error")

export default {
  subscribe,
  info,
  warn,
  error,
}

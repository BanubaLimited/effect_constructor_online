import type { BanubaSDK, Player } from "@banuba/webar"
import { exists } from "../utils/fs"
import { EffectManager } from "./effect-manager"
import { gltfAnimationEnd, gltfAnimationStart, makeupAPIend, makeupAPIstart } from "./makeup/utils"
import { codeEditorPlaceholder, codeEditorPlaceholderNoBreaks } from "./utils"

export class CodeEditor {
  private readonly _fs: typeof FS
  private readonly _effectManager: EffectManager
  private readonly _configPath: string
  private _makeupImport: string
  private _gltfAnimation: string
  private _hasBeautification = true
  private _hasGltfAnimation = false

  constructor(player: Player) {
    this._configPath = `${player["_effectManager"].current().url()}/config.js`
    this._fs = player["_sdk"].FS
    this._effectManager = new EffectManager(player)
  }

  get code(): string | null {
    if (!exists(this._fs, this._configPath)) return null
    const file = this._fs.readFile(this._configPath)
    let script = new TextDecoder().decode(file)

    if (!script.includes(codeEditorPlaceholderNoBreaks)) {
      script = codeEditorPlaceholder + script
    }

    if (!script.includes(makeupAPIstart)) {
      this._hasBeautification = false
      return script
    }

    if (script.includes(gltfAnimationStart)) {
      this._hasGltfAnimation = true
    }

    const makeupAPIidxSrart = script.indexOf(makeupAPIstart)
    const makeupAPIidxEnd = script.indexOf(makeupAPIend)

    this._makeupImport = script.substring(makeupAPIidxSrart, makeupAPIidxEnd + makeupAPIend.length)

    let clearScript =
      script.substring(0, makeupAPIidxSrart) +
      script.substring(makeupAPIidxEnd + makeupAPIend.length)

    const gltfAnimationIdxStart = clearScript.indexOf(gltfAnimationStart)
    const gltfAnimationIdxEnd = clearScript.indexOf(gltfAnimationEnd)

    this._gltfAnimation = clearScript.substring(
      gltfAnimationIdxStart,
      gltfAnimationIdxEnd + gltfAnimationEnd.length
    )

    clearScript =
      clearScript.substring(0, gltfAnimationIdxStart) +
      clearScript.substring(gltfAnimationIdxEnd + gltfAnimationEnd.length)

    return clearScript
  }

  async save(code: string) {
    code = `${this._hasBeautification ? this._makeupImport : ""}${
      this._hasGltfAnimation ? this._gltfAnimation : ""
    }${code}`
    this._fs.writeFile(this._configPath, code)
    await this._effectManager.execute((effect: BanubaSDK.Effect) => null)
  }
}

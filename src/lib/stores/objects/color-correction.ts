import type { Player, BanubaSDK } from "@banuba/webar"
import { rmrf, writeFile, readDirRecursive, exists } from "../utils/fs"

const COLOR_CORRECTION_ROOT_FOLDER = "color"

export class ColorCorrection {
  private readonly _player: Player
  private readonly _currentEffect: BanubaSDK.Effect
  private readonly _fs: typeof FS
  private _filterTexture: string | null = null
  private _isFilterActive = true

  constructor(player: Player) {
    this._player = player
    this._currentEffect = player["_effectManager"].current()
    this._fs = player["_sdk"].FS
    this._addFilterFilePath()
  }

  async setTexture(file: File | string) {
    if (typeof file === "string") {
      const url = await fetch(file)
      file = new File([await url.blob()], file.split("/").pop())
    }

    const url = this._player["_effectManager"].current().url()
    const relativePath = `${COLOR_CORRECTION_ROOT_FOLDER}/${file.name}`
    const absolutePath = `${url}/${relativePath}`
    const folderPath = `${url}/${COLOR_CORRECTION_ROOT_FOLDER}`

    /*
     * FIXME: seems like filter path is beign cashed, only with "new" path filter is applied
     * rmrf color folder in case we don't know previous paths
     */
    rmrf(this._fs, folderPath)
    await writeFile(this._fs, absolutePath, file)

    this._player._evalJs(`Filter.set("${relativePath}");`)

    this._filterTexture = relativePath
  }

  setFilterActive(value: boolean) {
    if (!value) {
      this._player._evalJs(`Filter.clear();`)
    }

    if (value && this._filterTexture) {
      this._player._evalJs(`Filter.set("${this._filterTexture}");`)
    }

    this._isFilterActive = value
  }

  isFilterActive() {
    return this._isFilterActive
  }

  getFilterTextureName(): string {
    return this._filterTexture?.split("/").pop() ?? ""
  }

  clear() {
    this._player._evalJs(`Filter.clear();`)
    this._filterTexture = null
    const url = this._player["_effectManager"].current().url()
    rmrf(this._fs, `${url}/${COLOR_CORRECTION_ROOT_FOLDER}`)
  }

  private _addFilterFilePath = () => {
    const absolutePath = `${this._currentEffect.url()}/${COLOR_CORRECTION_ROOT_FOLDER}`

    if (exists(this._fs, absolutePath)) {
      Object.keys(readDirRecursive(this._fs, absolutePath)).forEach((fileName) => {
        const idx = fileName.indexOf(COLOR_CORRECTION_ROOT_FOLDER)
        const relativePath = fileName.substring(idx)
        this._filterTexture = relativePath
        this._isFilterActive = true
      })
    }
  }
}

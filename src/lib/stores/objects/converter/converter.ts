import { wrap } from "comlink"
import { writable } from "svelte/store"
import ConverterWorker from "./converter.worker?worker"

export const gltfLoading = writable<boolean>(false)
let generate

export const generateEffectFromGltf = async (gltf: File, physics: boolean) => {
  if (!generate) generate = wrap(new ConverterWorker())

  gltfLoading.set(true)
  const res = await generate(gltf, physics)
  gltfLoading.set(false)

  return res
}

export const deleteConverter = () => {
  generate = undefined
}

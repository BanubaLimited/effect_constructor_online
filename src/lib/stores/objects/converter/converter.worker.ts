import { expose } from "comlink"
import mapKeys from "lodash/mapKeys"
import { nanoid } from "nanoid"
import { simd as isSimdSupported } from "wasm-feature-detect"
import gltf_bsm2 from "$lib/converter/gltf_bsm2"
import { UnsupportedFormatException, UnsupportedGLTFobjectException } from "$lib/global/exceptions"
import { writeDir, rmrf, readDirRecursive } from "../../utils/fs"
import { zip, unzip } from "../../utils/file-reader"
import { wrapGltfAnimation } from "../../objects/makeup/utils"
import wasm from "$lib/converter/gltf_bsm2.wasm?url"
import simd from "$lib/converter/simd/gltf_bsm2.wasm?url"
import data from "$lib/converter/gltf_bsm2.data?url"

const MODEL_TYPE = /\.gl(tf|b)/

interface GLTFBSM2Converter extends EmscriptenModule {
  generateEffect(
    inputFile: string,
    outDir: string,
    onComplete: (hasError: boolean, infoMsg: string) => void
  )
  generateEffectMulti(
    inputFiles: { input: string; physics: boolean }[],
    outDir: string,
    onComplete: (hasError: boolean, infoMsg: string) => void
  )
  mergeConvertedEffects(
    inputFiles: { input: string }[],
    outDir: string,
    onComplete: (hasError: boolean, infoMsg: string) => void
  )
}

class Converter {
  private readonly _fs: typeof FS
  private readonly _converter: GLTFBSM2Converter
  private static _instance: Converter

  // as mergeConvertedEffects accepts Emscripten`s const val& and do e["input"].as<std::string>()
  // we need to provide object with 'input' field
  private _convertedModels: { input: string }[]

  private constructor(converter: GLTFBSM2Converter) {
    this._fs = converter["FS"]
    this._converter = converter
  }

  static delete() {
    this._instance = null
  }

  static async create() {
    if (this._instance) return this._instance

    const isSupported = await isSimdSupported()

    const gltf2bsm = await gltf_bsm2({
      locateFile: (fname) => {
        if (fname.endsWith(".data")) return data

        if (isSupported) {
          console.info("Using simd.wasm for gltf_bsm")
          return simd
        } else {
          console.info("Using wasm for gltf_bsm")
          return wasm
        }
      },
    })

    return (this._instance = new Converter(gltf2bsm))
  }

  async generate(archive: File, physics: boolean): Promise<Blob> {
    const ROOT_DIR = "models"
    const files = await unzip(archive)
    const modelBin = Object.keys(files).find((filename) => MODEL_TYPE.test(filename))

    if (!modelBin) {
      throw new UnsupportedFormatException("Model archive has no .gltf/.glb files")
    }

    const cwd = `${ROOT_DIR}/${nanoid()}`
    const cwdOut = `${ROOT_DIR}/${nanoid()}`
    const input = `${cwd}/${modelBin}`
    writeDir(this._fs, cwd, files)

    const mergedEffectOutput = nanoid()

    try {
      await new Promise((resolve, reject) =>
        this._converter.generateEffectMulti(
          [{ input, physics }],
          cwdOut,
          (error: boolean, success: string) => {
            if (error) reject(error)
            else {
              rmrf(this._fs, input)
              if (!this._convertedModels) this._convertedModels = [{ input: cwdOut }]
              else this._convertedModels.push({ input: cwdOut })
              resolve(success)
            }
          }
        )
      )

      await new Promise((resolve, reject) =>
        this._converter.mergeConvertedEffects(
          this._convertedModels,
          mergedEffectOutput,
          (error: boolean, success: string) => {
            if (error) reject(error)
            else resolve(success)
          }
        )
      )

      const files = readDirRecursive(this._fs, mergedEffectOutput)
      const extensions = /\.[0-9a-z]+$/i
      const readyFiles = mapKeys(files, (_, key) => (extensions.test(key) ? key : `${key}.png`))

      const configJsonPath = `${mergedEffectOutput}/config.json`
      const configJsPath = `${mergedEffectOutput}/config.js`
      const configJson = JSON.parse(new TextDecoder().decode(readyFiles[configJsonPath]))
      configJson.entities["__internal_frx_face"].active = true

      configJson["script"] = {
        entry_point: "config.js",
        type: "latest",
      }

      const textEncoder = new TextEncoder()
      readyFiles[configJsonPath] = textEncoder.encode(JSON.stringify(configJson, null, 2))
      readyFiles[configJsPath] = physics
        ? textEncoder.encode(JSON.stringify("// Your code starts here"))
        : textEncoder.encode(this._createAnimationScript(configJson.entities))

      return zip(readyFiles)
    } catch (e) {
      console.warn(e)
      throw new UnsupportedGLTFobjectException(
        "This model can't be converted, please try another one"
      )
    } finally {
      rmrf(this._fs, mergedEffectOutput)
    }
  }

  private _createAnimationScript(entities) {
    const generateScript = (
      entityName: string
    ) => `const gltf_${entityName} = bnb.scene.getRoot().findChildByName('${entityName}')?.getComponent(bnb.ComponentType.MESH_INSTANCE)?.asMeshInstance();\n
    gltf_${entityName}?.animationPlay();\n`

    let code = ""
    Object.keys(entities).forEach((entity) => {
      if (!entity.startsWith("_") && !entity.startsWith("camera") && !entity.startsWith("face"))
        code += generateScript(entity)
    })

    return wrapGltfAnimation(code)
  }
}

const generate = async (gltf: File, physics: boolean) => {
  const converter = await Converter.create()
  return await converter.generate(gltf, physics)
}

expose(generate)

import type { BanubaSDK } from "@banuba/webar"
import { writable } from "svelte/store"
import { Player } from "@banuba/webar"
import _ from "lodash"
import { nanoid } from "nanoid"
import { exists, differenceDump, writeDir, rmrf, addScriptField } from "$lib/stores/utils/fs"
import { getMakeup, makeupAPIstart, wrapMakeupImport } from "./makeup/utils"
import { codeEditorPlaceholder } from "./utils"

const { mapValues } = _

export const effectLoading = writable<boolean>(false)

export const loading = () => {
  return (_, __, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value

    descriptor.value = async function (...args: any[]) {
      effectLoading.set(true)
      const result = await originalMethod.apply(this, args)
      effectLoading.set(false)
      return result
    }
  }
}

type SceneCommand = (effect: BanubaSDK.Effect, ...args: any[]) => any
export class EffectManager {
  private readonly _player: Player
  private readonly _effectManager: BanubaSDK.EffectManager
  private readonly _canvas
  private readonly _isGltf: boolean
  private readonly _textDecoder = new TextDecoder()
  private readonly _textEncoder = new TextEncoder()

  constructor(player: Player, isGltf?: boolean) {
    this._player = player
    this._effectManager = this._player["_effectManager"]
    this._canvas = this._player["_sdk"].canvas
    this._isGltf = isGltf
  }

  @loading()
  async execute(command: SceneCommand, ...args: any[]): Promise<void> {
    const previous = this._effectManager.current()
    const fs = this._player["_sdk"].FS

    const dumpPath = `/tmp/${nanoid()}`
    const configJsonPath = `${dumpPath}/config.json`
    const configJsPath = `${dumpPath}/config.js`

    previous.dumpFs(dumpPath)

    this._removeSound()
    const next = this._effectManager.createEffect("")

    differenceDump(previous.url(), dumpPath, this._player["_sdk"].FS)
    const configJson = JSON.parse(this._textDecoder.decode(fs.readFile(configJsonPath)))

    configJson["script"] = {
      entry_point: "config.js",
      type: "latest",
    }

    if (!exists(fs, configJsPath)) {
      fs.writeFile(configJsPath, "")
    }

    fs.writeFile(configJsonPath, JSON.stringify(configJson, null, 2))

    next.deserialize(dumpPath)
    await command(next, ...args)

    next.activate(this._canvas.width, this._canvas.height, this._canvas.width, this._canvas.height)

    this._effectManager.setCurrentEffect(next)

    this._player.dispatchEvent(new CustomEvent(Player.EFFECT_ACTIVATED_EVENT, { detail: next }))
  }

  @loading()
  clearScripting(effect: BanubaSDK.Effect): void {
    if (this._isGltf) return

    const fs = this._player["_sdk"].FS

    const dumpPath = `/tmp/${nanoid()}`

    const configJsonPath = `${dumpPath}/config.json`
    const configJsPath = `${effect.url()}/config.js`

    if (!exists(fs, configJsPath)) return

    effect.dumpFs(dumpPath)

    const configJson = JSON.parse(this._textDecoder.decode(fs.readFile(configJsonPath)))
    const configJs = this._textDecoder.decode(fs.readFile(configJsPath))

    if (
      configJs.includes(makeupAPIstart) ||
      (configJson.script && configJson.script?.type !== "legacy")
    ) {
      return
    }
    addScriptField(fs, dumpPath)

    // FIME: sdk bug, all entities are inactive after dumpFS at first time
    const entities = configJson.entities
    configJson.entities = mapValues(entities, (entity) => ({ ...entity, active: true }))

    fs.writeFile(`${dumpPath}/config.js`, codeEditorPlaceholder)
    fs.writeFile(configJsonPath, JSON.stringify(configJson, null, 2))

    this._removeSound()
    const next = this._effectManager.createEffect("")
    next.deserialize(dumpPath)
    next.activate(this._canvas.width, this._canvas.height, this._canvas.width, this._canvas.height)

    this._effectManager.setCurrentEffect(next)

    this._player.dispatchEvent(new CustomEvent(Player.EFFECT_ACTIVATED_EVENT, { detail: next }))
  }

  @loading()
  async createBlankEffect(): Promise<void> {
    const files = await getMakeup()
    const makeupPath = `/tmp/makeup`
    const fs = this._player["_sdk"].FS

    const configJs = this._textDecoder.decode(files["config.js"])
    if (!configJs.includes(makeupAPIstart)) {
      files["config.js"] = this._textEncoder.encode(wrapMakeupImport(configJs))
    }

    if (exists(fs, makeupPath)) rmrf(fs, makeupPath)

    writeDir(fs, makeupPath, files)

    this._removeSound()

    const next = this._effectManager.createEffect("")
    next.deserialize(makeupPath)
    next.activate(this._canvas.width, this._canvas.height, this._canvas.width, this._canvas.height)
    this._effectManager.setCurrentEffect(next)

    this._player.dispatchEvent(new CustomEvent(Player.EFFECT_ACTIVATED_EVENT, { detail: next }))
  }

  @loading()
  async setMakeup(): Promise<boolean> {
    const fs = this._player["_sdk"].FS
    const configJSPath = `${this._player["_effectManager"].current().url()}/config.js`

    let configJs = ""

    if (exists(fs, configJSPath)) {
      configJs = this._textDecoder.decode(fs.readFile(configJSPath))
    }

    if (configJs.includes(makeupAPIstart)) return true

    const files = await getMakeup()

    const code = this._textDecoder.decode(files["config.js"])

    delete files["config.js"]
    delete files["config.json"]

    fs.writeFile(configJSPath, wrapMakeupImport(code) + configJs)

    writeDir(fs, this._effectManager.current().url(), files)

    await this.execute((_: BanubaSDK.Effect) => null)
    return true
  }

  setEffectVolume(value: number): void {
    this._effectManager.setEffectVolume(value)
  }

  private _removeSound(): void {
    this._effectManager.setEffectVolume(0)
  }
}

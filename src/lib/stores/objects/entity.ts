import type { BanubaSDK, Player } from "@banuba/webar"
import { TinyEmitter } from "tiny-emitter"
import { vectorToArray, degrees, radians } from "./utils"
import { findMaterialName, setTexture } from "./material-utils"
import { EffectManager } from "./effect-manager"

const reserved = [/^camera$/, /^ComposerRT/]
export class Entity extends TinyEmitter {
  private readonly _sdk: typeof BanubaSDK
  private readonly _effectManager: EffectManager
  private readonly _player: Player
  private readonly _name: string
  readonly children: Entity[]

  constructor(player: Player, entity: BanubaSDK.Entity) {
    super()
    this._sdk = player["_sdk"]
    this._name = entity.getName()
    this._player = player
    this._effectManager = new EffectManager(player)

    this.children = vectorToArray(entity.getChildren())
      .map((entity) => new Entity(player, entity))
      .filter((entity) => reserved.every((reserved) => !reserved.test(entity.title)))

    this.children.forEach((child) => {
      child.once("remove", (details) => {
        this.emit("remove", details)
      })
    })
  }

  get title() {
    return this._entity.getName()
  }
  set title(value: string) {
    this._entity.setName(value)
    this._player._evalJs(``)
  }

  get active() {
    return this._entity.isActive()
  }
  set active(value: boolean) {
    this._entity.setActive(value)
    this._player._evalJs(``)
  }

  get _transform(): BanubaSDK.Transformation3d | null {
    return this._entity.getComponent(this._sdk.ComponentType.TRANSFORMATION)?.asTransformation()
  }

  get _meshInstance(): BanubaSDK.MeshInstance | null {
    return this._entity.getComponent(this._sdk.ComponentType.MESH_INSTANCE)?.asMeshInstance()
  }

  get translation() {
    return this._transform.getTranslation()
  }
  set translation(value: BanubaSDK.Vec3) {
    this._transform.setTranslation(value)
    this._player._evalJs(``)
  }

  get materials() {
    const vector = this._meshInstance?.getMaterials()

    if (vector) return vectorToArray(vector)
    else return []
  }

  get rotation() {
    const { x, y, z } = this._transform.getRotation()

    return {
      x: degrees(x),
      y: degrees(y),
      z: degrees(z),
    }
  }
  set rotation(value: BanubaSDK.Vec3) {
    const [x, y, z] = [radians(value.x), radians(value.y), radians(value.z)]
    this._transform.setRotation(new this._sdk.Vec3(x, y, z))
    this._player._evalJs(``)
  }

  get scale() {
    return this._transform.getScale()
  }
  set scale(value: BanubaSDK.Vec3) {
    this._transform.setScale(value)
    this._player._evalJs(``)
  }

  get _entity() {
    const effect = this._player["_effectManager"].current()
    const scene = effect.scene()
    const root = scene.getRoot()
    return root.findChildByName(this._name)
  }

  async setTexture(name: string, file: File): Promise<void> {
    await this._effectManager.execute(setTexture, this._entity, this._sdk, file, name)
  }

  findMaterialName(name: string): string | null {
    return findMaterialName(name, this.materials)
  }

  /**
   * Indicates whether an entity is a descendant of a given entity,
   * i.e. the entity itself, one of its direct children, one of the children's direct children, and so on.
   */
  contains(entity: Entity): boolean {
    return (
      entity?._entity?.getName() === this?._entity?.getName() ||
      this.children.some((c) => c.contains(entity))
    )
  }

  remove(): void {
    this.active = false
    this._entity.getParent().removeChild(this._entity)
    this.emit("remove", this)
  }
}

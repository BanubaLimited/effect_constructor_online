import { nanoid } from "nanoid"
import mapKeys from "lodash/mapKeys"
import envFilter from "$lib/env-filter/env_filter.js"
import wasm from "$lib/env-filter/env_filter.wasm?url"
import { write, rmrf, readDirRecursive } from "../utils/fs"

export class EnvFilter {
  private readonly _fs: typeof FS
  private readonly _envFilter: EmscriptenModule
  private static _instance: EnvFilter

  private constructor(envFilter: EmscriptenModule) {
    this._fs = envFilter["FS"]
    this._envFilter = envFilter
  }

  static async create(): Promise<EnvFilter> {
    if (EnvFilter._instance) return EnvFilter._instance

    const _envFilter = await envFilter({
      locateFile: () => wasm,
    })

    // maybe move to sdk ?
    const { GL } = _envFilter
    const canvas = document.createElement("canvas")
    const ctx = canvas.getContext("webgl2")
    const handle = GL.registerContext(ctx, {})
    GL.makeContextCurrent(handle)

    return (EnvFilter._instance = new EnvFilter(_envFilter))
  }

  async process(file: File): Promise<Record<string, Uint8Array>> {
    const cwd = `tmp/${nanoid()}`
    const src = `${cwd}/src`
    const dist = `${cwd}/dist`

    const hdr = `${src}/${file.name}`

    write(this._fs, hdr, new Uint8Array(await file.arrayBuffer()))

    // TODO: move to sdk
    this._fs.mkdir(dist)

    try {
      this._envFilter.processHDR(hdr, dist)
      const files = readDirRecursive(this._fs, dist)
      return mapKeys(files, (_, key) => key.replace(dist + "/", ""))
    } finally {
      rmrf(this._fs, hdr, dist)
    }
  }
}

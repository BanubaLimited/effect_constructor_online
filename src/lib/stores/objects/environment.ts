import _ from "lodash"
import type { Player, BanubaSDK } from "@banuba/webar"
import { writeFile, readDirRecursive, exists, rmrf } from "../utils/fs"
import { removeBackgroundTextureImport, updateBackgroundTextureImport } from "./makeup/utils"

const { capitalize } = _

export const ENV_FEATURES = {
  background: "Background.texture",
  foreground: "Foreground.texture",
  cubemap: null,
}

const ENVIRONMENT_ROOT_FOLDER = "environment"

export class Environment {
  private readonly _currentEffect: BanubaSDK.Effect
  private readonly _player: Player
  private readonly _fs: typeof FS
  private _textures = { background: null, foreground: null, cubemap: null }
  private _blendingMode = "normal"
  private _activeEnvFeatures = { background: true, foreground: true, cubemap: true }

  constructor(player: Player) {
    this._player = player
    this._currentEffect = player["_effectManager"].current()
    this._fs = player["_sdk"].FS

    this._player._evalJs(`Background.clear();`)
    this._player._evalJs("Background.contentMode('fill')")
    this._addEnvironmentFilesPaths()
  }

  async setTexture(file: File | string, envFeature: keyof typeof ENV_FEATURES): Promise<void> {
    if (typeof file === "string") {
      const url = await fetch(file)
      file = new File([await url.blob()], file.split("/").pop())
    }

    /**
     * await this._currentEffect.writeFile(path, await file.arrayBuffer())
     * does not work on bindigns effect
     */
    const url = this._player["_effectManager"].current().url()
    const fs = this._player["_sdk"].FS

    const folderPath = `${ENVIRONMENT_ROOT_FOLDER}/${envFeature}`
    const relativePath = `${folderPath}/${file.name}`
    const absolutePath = `${url}/${relativePath}`

    rmrf(fs, `${url}/${folderPath}`)
    await writeFile(fs, absolutePath, file)

    if (envFeature === "background") {
      updateBackgroundTextureImport(fs, this._currentEffect.url(), relativePath)
    }

    this._player._evalJs(`${ENV_FEATURES[envFeature]}("${relativePath}");`)
    this._textures[envFeature] = relativePath
  }

  setBlendingMode(
    mode?: "normal" | "multiply" | "screen" | "overlay" | "soft_light" | "hard_light"
  ) {
    this._player._evalJs(`Foreground.blendMode("${mode}");`)
    this._blendingMode = mode
  }

  getBlendingMode() {
    return this._blendingMode
  }

  setEnvironmentFeatureActive(envFeature: keyof typeof ENV_FEATURES, value: boolean): void {
    if (!value) {
      this._player._evalJs(`${capitalize(envFeature)}.clear();`)
    }

    if (value && this._textures[envFeature]) {
      this._player._evalJs(`${ENV_FEATURES[envFeature]}("${this._textures[envFeature]}");`)
    }

    if (value && envFeature === "foreground") {
      this._player._evalJs(`Foreground.blendMode("${this._blendingMode}");`)
    }

    this._activeEnvFeatures[envFeature] = value
  }

  getTextureName(envFeature: keyof typeof ENV_FEATURES): string {
    return this._textures[envFeature]?.split("/").pop() ?? ""
  }

  isEnvironmentFeatureActive(envFeature: keyof typeof ENV_FEATURES) {
    return this._activeEnvFeatures[envFeature]
  }

  clear(envFeature: keyof typeof ENV_FEATURES) {
    this._player._evalJs(`${capitalize(envFeature)}.clear();`)
    this._textures[envFeature] = null
    this._blendingMode = "normal"
    const url = this._player["_effectManager"].current().url()
    const absolutePath = `${url}/${ENVIRONMENT_ROOT_FOLDER}/${envFeature}`
    rmrf(this._fs, absolutePath)

    if (envFeature === "background") removeBackgroundTextureImport(this._fs, url)
  }

  private _addEnvironmentFilesPaths = () => {
    const absolutePath = `${this._currentEffect.url()}/${ENVIRONMENT_ROOT_FOLDER}`

    if (exists(this._fs, absolutePath)) {
      Object.keys(readDirRecursive(this._fs, absolutePath)).forEach((fileName) => {
        const idx = fileName.indexOf(ENVIRONMENT_ROOT_FOLDER)
        const relativePath = fileName.substring(idx)
        const featureName = relativePath.split("/")[1]

        if (featureName in this._textures) {
          this._textures[featureName] = relativePath
          this._activeEnvFeatures[featureName] = true
          this._player._evalJs(`${ENV_FEATURES[featureName]}("${relativePath}");`)
        }
      })
    }

    new Promise((r, _) =>
      this._player._evalJs(`Foreground.blendMode();`).then((mode) => {
        this._blendingMode = mode
        r(mode)
      })
    ).then((value) => {
      this._blendingMode = value as string
    })
  }
}

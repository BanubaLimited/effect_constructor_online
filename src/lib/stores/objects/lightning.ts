import type { Player, BanubaSDK } from "@banuba/webar"
import { EffectManager } from "./effect-manager"
import { EnvFilter } from "./env-filter"
import { setLightning } from "./material-utils"

export class Lightning {
  private readonly _sdk: typeof BanubaSDK
  private readonly _effectManager: EffectManager
  private readonly _envFilter: Promise<EnvFilter>
  private _materials = { iblDiff: "", iblSpec: "" }

  constructor(player: Player) {
    this._sdk = player["_sdk"]
    this._effectManager = new EffectManager(player)
    this._envFilter = EnvFilter.create()
  }

  async importHDR(file: File) {
    const envFilter = await this._envFilter

    const textures = await envFilter.process(file)

    await this._effectManager.execute(setLightning, this._sdk.FS, textures)

    this._materials["iblDiff"] = file.name
  }

  get materials() {
    return this._materials
  }
}

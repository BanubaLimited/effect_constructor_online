import _ from "lodash"
import { unzip } from "$lib/stores/utils/file-reader"
import type { BanubaSDK } from "@banuba/webar"
import { writeDir } from "$lib/stores/utils/fs"
import { EffectManager, loading } from "../effect-manager"
import { UnsupportedFormatException } from "$lib/global/exceptions"

const { mapKeys } = _

const FILE_VERT = /\.vert/
const FILE_FRAG = /\.frag/

export class Shader {
  private readonly _player: BanubaSDK.Player
  private readonly _fs: typeof FS
  private _defaultShaders: File
  private _activeCameraShader: File | null
  private _active = true
  private static _instance: Shader

  private constructor(player: BanubaSDK.Player) {
    this._player = player
    this._fs = player["_sdk"].FS
  }

  private static _move(player: BanubaSDK.Player, from: Shader) {
    const instance = new Shader(player)
    instance._active = from._active
    instance._activeCameraShader = from._activeCameraShader
    instance._defaultShaders = from._defaultShaders
    return (Shader._instance = instance)
  }

  static create(player: BanubaSDK.Player): Shader {
    if (Shader._instance) return Shader._move(player, Shader._instance)
    return (Shader._instance = new Shader(player))
  }

  @loading()
  async setCameraShader(archive: File | string, defaultShader = false) {
    if (!defaultShader) this._activeCameraShader = archive

    if (typeof archive === "string") {
      const url = await fetch(archive)
      archive = new File([await url.blob()], archive.split("/").pop())
    }

    const files = await unzip(archive)

    const vert = Object.keys(files).find((filename) => FILE_VERT.test(filename))
    const frag = Object.keys(files).find((filename) => FILE_FRAG.test(filename))

    if (!vert || !frag) {
      throw new UnsupportedFormatException("Archive has no .vert/.frag files")
    }

    const url = `${this._player["_effectManager"].current().url()}/modules/camera-shader`

    const readyFiles = mapKeys(files, (_, key) => `camera_shader.${key.split(".").pop()}`)

    writeDir(this._fs, url, readyFiles)

    const effectManager = new EffectManager(this._player)
    await effectManager.execute((effect: BanubaSDK.Effect) => null)
  }

  @loading()
  async clearShaders() {
    this._activeCameraShader = null
    await this.deactivate()
  }

  @loading()
  async deactivate() {
    this._active = false
    if (!this._defaultShaders) {
      const url = await fetch("/pressets/shaders/default_shaders.zip")
      const file = new File([await url.blob()], "default_shaders.zip")
      this._defaultShaders = file
    }

    await this.setCameraShader(this._defaultShaders, true)
  }

  @loading()
  async activate() {
    this._active = true
    if (this._activeCameraShader) await this.setCameraShader(this._activeCameraShader)
  }

  isActive() {
    return this._active
  }
}

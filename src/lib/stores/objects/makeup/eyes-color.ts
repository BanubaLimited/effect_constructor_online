import { parseColorFromEvalJs } from "./utils"
import type { Player } from "@banuba/webar"
import type { ColorPaletteValue } from "./utils"

export class EyesColor {
  private readonly _player: Player
  private _currentColor: ColorPaletteValue
  private _currentColorVectorString: string | null
  private _isEyesColorActive = true

  constructor(player: Player) {
    this._player = player
    this._obtainEyesColor()
  }

  setColor(color: string) {
    this._player._evalJs(`Eyes.color("${color}");`)
    this._currentColorVectorString = color
  }

  getCurrentColor() {
    return this._currentColor
  }

  setCurrentColor(color: ColorPaletteValue) {
    this._currentColor = color
  }

  setEyesColorActive(value: boolean) {
    if (!value) {
      this._player._evalJs(`Eyes.clear();`)
    }

    if (value && this._currentColorVectorString) {
      this.setColor(this._currentColorVectorString)
    }

    this._isEyesColorActive = value
  }

  getEyesColorActive() {
    return this._isEyesColorActive
  }

  clear() {
    this._player._evalJs(`Eyes.clear();`)
    this._currentColor = parseColorFromEvalJs(null)
    this._currentColorVectorString = "0 0 0 0"
  }

  private _obtainEyesColor(): void {
    this._player._evalJs(`Eyes.color();`).then((color) => {
      if (color !== "null") {
        this._currentColorVectorString = color
        this._currentColor = parseColorFromEvalJs(color)
      }
    })
  }
}

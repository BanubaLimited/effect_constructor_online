import type { Player } from "@banuba/webar"
export class FaceShape {
  private readonly _player: Player
  private _active = true
  private _wasSet = false
  private _shapes = {
    face: 0,
    eyes: 0,
    nose: 0,
    lips: 0,
  }

  get shapes() {
    return this._shapes
  }

  get active() {
    return this._active
  }

  setActive(value: boolean) {
    if (!value) this.clear()
    else this.activate()

    this._active = value

    if (!this._wasSet && value === false) this._wasSet = true
  }

  constructor(player: Player) {
    this._player = player
    this._obtainFaceShapeValues()
  }

  face(value: number): void {
    this._shapes.face = value
    if (!this.active) return
    this._player._evalJs(`FaceMorph.face(${value});`)
  }

  eyes(value: number): void {
    this._shapes.eyes = value
    if (!this.active) return
    this._player._evalJs(`FaceMorph.eyes(${value});`)
  }

  nose(value: number): void {
    this._shapes.nose = value
    if (!this.active) return
    this._player._evalJs(`FaceMorph.nose(${value});`)
  }

  lips(value: number): void {
    this._shapes.lips = value / 10
    if (!this.active) return
    this._player._evalJs(`FaceMorph.lips(${value});`)
  }

  clear(entirely?: boolean) {
    this._player._evalJs(`FaceMorph.clear()`)

    if (entirely) Object.keys(this._shapes).forEach((key) => (this._shapes[key] = 0))
  }

  activate() {
    if (!this._wasSet) return

    Object.keys(this._shapes).forEach((key) =>
      this._player._evalJs(`FaceMorph.${key}(${this._shapes[key]});`)
    )
  }

  private _obtainFaceShapeValues(): void {
    Object.keys(this._shapes).forEach((shape) => {
      this._player._evalJs(`FaceMorph.${shape}();`).then((value) => {
        this._shapes[shape] = parseFloat(value)
      })
    })
  }
}

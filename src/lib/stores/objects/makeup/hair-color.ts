import { parseColorFromEvalJs } from "./utils"
import type { Player } from "@banuba/webar"
import type { ColorPaletteValue } from "./utils"

export class HairColor {
  private readonly _player: Player
  private _currentColor: ColorPaletteValue
  private _currentColorVectorString: string | null
  private _isHairColorActive = true

  constructor(player: Player) {
    this._player = player
    this._obtainHairColor()
  }

  setColor(color: string) {
    this._player._evalJs(`Hair.color("${color}");`)
    this._currentColorVectorString = color
  }

  getCurrentColor() {
    return this._currentColor
  }

  setCurrentColor(color: ColorPaletteValue) {
    this._currentColor = color
  }

  setHairColorActive(value: boolean) {
    if (!value) {
      this._player._evalJs(`Hair.clear();`)
    }

    if (value && this._currentColorVectorString) {
      this.setColor(this._currentColorVectorString)
    }

    this._isHairColorActive = value
  }

  getHairColorActive() {
    return this._isHairColorActive
  }

  clear() {
    this._player._evalJs(`Hair.clear();`)
    this._currentColor = parseColorFromEvalJs(null)
    this._currentColorVectorString = "0 0 0 0"
  }

  private _obtainHairColor(): void {
    this._player._evalJs(`Hair.color();`).then((color) => {
      // while gradient feature is not active
      if (color !== "null") {
        color = color.split(",")[0]
        this._currentColorVectorString = color
        this._currentColor = parseColorFromEvalJs(color)
      }
    })
  }
}

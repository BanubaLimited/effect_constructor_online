import { parseColorFromEvalJs } from "./utils"
import type { Player } from "@banuba/webar"
import type { ColorPaletteValue } from "./utils"

export type lipsColorFeatures = {
  color: string
  saturation: number
  brightness: number
  shineIntensity: number
  shineBleeding: number
  shineScale: number
}

const defaultLipsFeaturesValues: lipsColorFeatures = {
  color: "0 0 0 0",
  saturation: 1,
  brightness: 1,
  shineIntensity: 1,
  shineBleeding: 1,
  shineScale: 0,
}

const shineFeatures = ["shineIntensity", "shineBleeding", "shineScale"] as const

export class LipsColor {
  private readonly _player: Player
  private _currentColor: ColorPaletteValue
  private _isLipsColorActive = true
  private _isShineActive = true
  private _featureValues: lipsColorFeatures = { ...defaultLipsFeaturesValues }

  constructor(player: Player) {
    this._player = player
    this._obtainLipsColor()
    this._setDefaultShineOptions()
  }

  setLipsColorProperty(property: keyof lipsColorFeatures, param: string | number, shine?: boolean) {
    //@ts-ignore
    this._featureValues[property] = param

    if (property in shineFeatures && !shine) {
      return
    }

    this._player._evalJs(`Lips.${property}("${param}");`)
  }

  get featureValues() {
    return this._featureValues
  }

  getCurrentColor() {
    return this._currentColor
  }

  setCurrentColor(color: ColorPaletteValue) {
    this._currentColor = color
  }

  setLipsColorActive(value: boolean) {
    if (!value) {
      this._player._evalJs(`Lips.clear();`)
    }

    if (value) {
      Object.keys(this._featureValues).forEach((key) => {
        this.setLipsColorProperty(key as keyof lipsColorFeatures, this._featureValues[key])
      })
    }

    this._isLipsColorActive = value
  }

  getLipsColorActive() {
    return this._isLipsColorActive
  }

  setShineActive(value: boolean) {
    if (!value) {
      this._player._evalJs(`Lips.shineScale(0);`)
    } else {
      this.setLipsColorProperty("shineScale", this._featureValues.shineScale, true)
    }

    this._isShineActive = value
  }

  getShineActive() {
    return this._isShineActive
  }

  clear() {
    this._player._evalJs(`Lips.clear();`)
    this._currentColor = parseColorFromEvalJs(null)
    this._featureValues = { ...defaultLipsFeaturesValues }
    this._setDefaultShineOptions()
  }

  private _obtainLipsColor(): void {
    Object.keys(this._featureValues).forEach((feature) => {
      this._player._evalJs(`Lips.${feature}();`).then((val: string | number) => {
        if (feature !== "color" && val !== "null") val = parseFloat(val as string)
        if (feature === "color" && val) this._currentColor = parseColorFromEvalJs(val as string)
        if (val !== "null") this._featureValues[feature] = val
      })
    })
  }

  private _setDefaultShineOptions(): void {
    if (this._featureValues.shineIntensity <= 1)
      this.setLipsColorProperty("shineIntensity", defaultLipsFeaturesValues.shineIntensity, true)
    if (this._featureValues.shineBleeding <= 1)
      this.setLipsColorProperty("shineBleeding", defaultLipsFeaturesValues.shineBleeding, true)
  }
}

import type { Player } from "@banuba/webar"

const FACTOR = 3

export class Retouch {
  private readonly _player: Player
  private _active = true
  private _wasSet = false
  private _shapes = {
    Skin: { softening: 0 },
    Teeth: { whitening: 0 },
    Eyes: { whitening: 0 },
  }

  constructor(player: Player) {
    this._player = player
    this._obtainRetouchValues()
  }

  get shapes() {
    return this._shapes
  }

  get active() {
    return this._active
  }

  setActive(value: boolean) {
    if (!value) this.clear()
    else this.activate()

    this._active = value

    if (!this._wasSet && value === false) this._wasSet = true
  }

  skin(value: number): void {
    this._shapes["Skin"]["softening"] = value
    if (!this.active) return
    this._player._evalJs(`Skin.softening(${value * FACTOR});`)
  }

  teeth(value: number): void {
    this._shapes["Teeth"]["whitening"] = value
    if (!this.active) return
    this._player._evalJs(`Teeth.whitening(${value * FACTOR});`)
  }

  eyes(value: number): void {
    this._shapes["Eyes"]["whitening"] = value
    if (!this.active) return
    this._player._evalJs(`Eyes.whitening(${value * FACTOR});`)
  }

  clear(entirely?: boolean) {
    this._player._evalJs("Skin.softening(0);")
    this._player._evalJs("Teeth.whitening(0);")
    this._player._evalJs("Eyes.whitening(0);")

    if (entirely) {
      Object.keys(this._shapes).forEach((shape) =>
        Object.keys(this._shapes[shape]).forEach((key) => (this._shapes[shape][key] = 0))
      )
    }
  }

  activate() {
    if (!this._wasSet) return

    this._player._evalJs(`Skin.softening(${this._shapes.Skin.softening * (FACTOR + 0.1)});`)
    this._player._evalJs(`Teeth.whitening(${this._shapes.Teeth.whitening * (FACTOR + 0.1)});`)
    this._player._evalJs(`Eyes.whitening(${this._shapes.Eyes.whitening * (FACTOR + 0.1)});`)
  }

  private _obtainRetouchValues(): void {
    Object.keys(this._shapes).forEach((shape) => {
      Object.keys(this._shapes[shape]).forEach((feature) => {
        this._player._evalJs(`${shape}.${feature}();`).then((val) => {
          if (val !== "null") this._shapes[shape][feature] = parseFloat(val) / 3
        })
      })
    })
  }
}

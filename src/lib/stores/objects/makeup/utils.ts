import rgbHex from "rgb-hex"
import { Effect, type Player } from "@banuba/webar"
import type { BanubaSDK } from "@banuba/webar"

export const getMakeup = (() => {
  let files: Record<string, Uint8Array> = null
  const makeup = new Effect("/Makeup.zip")

  return async () => {
    if (!files) {
      await makeup["_load"]()
      files = makeup["_resource"]["_data"]
    }

    return { ...files }
  }
})()

export const makeupAPIstart = "/*@__MAKEUP_API_START__*/"
export const makeupAPIend = "/*@__MAKEUP_API_END__*/"

export const backgroundImportStart = "/*@__BACKGROUND_IMPORT_START__*/"
export const backgroundImportEnd = "/*@__BACKGROUND_IMPORT_END__*/"

export const gltfAnimationStart = "/*@__GLTF_ANIMATION_START__*/"
export const gltfAnimationEnd = "/*@__GLTF_ANIMATION_END__*/"

export const wrapMakeupImport = (code: string) => `${makeupAPIstart}\n${code}\n${makeupAPIend}`
export const wrapBackgroundTextureImport = (texturePath: string) =>
  `${backgroundImportStart}\nBackground.texture("${texturePath}")\n${backgroundImportEnd}`
export const wrapGltfAnimation = (code: string) =>
  `${gltfAnimationStart}\n${code}\n${gltfAnimationEnd}`

export const removeOldBackgroundTextureImport = (code: string) => {
  const startIndex = code.indexOf(backgroundImportStart)
  const endIndex = code.indexOf(backgroundImportEnd) + backgroundImportEnd.length

  if (startIndex === -1 || endIndex === -1) return code

  return code.substring(0, startIndex) + code.substring(endIndex)
}

export const removeBackgroundTextureImport = (fs: typeof FS, effectPath: string) => {
  const configJsPath = `${effectPath}/config.js`
  let configJs = new TextDecoder().decode(fs.readFile(configJsPath))

  configJs = removeOldBackgroundTextureImport(configJs)
  fs.writeFile(configJsPath, configJs)
}

export const updateBackgroundTextureImport = (
  fs: typeof FS,
  effectPath: string,
  texturePath: string
): void => {
  const configJsPath = `${effectPath}/config.js`
  let configJs = new TextDecoder().decode(fs.readFile(configJsPath))

  configJs = removeOldBackgroundTextureImport(configJs)

  const backgroundTextureImport = wrapBackgroundTextureImport(texturePath)
  const makeupImportEndIndex = configJs.indexOf(makeupAPIend)
  const codeWithBackgroundTextureImport = `${configJs.substring(
    0,
    makeupImportEndIndex
  )}\n${backgroundTextureImport}\n${configJs.substring(makeupImportEndIndex)}`

  fs.writeFile(configJsPath, codeWithBackgroundTextureImport)
}

export const isCameraComposerEffect = (player: Player): boolean => {
  const effect = player["_effectManager"].current() as BanubaSDK.Effect
  const scene = effect.scene()
  if (!scene) return

  const assetManager = scene.getAssetManager()
  return !!(assetManager.findImage("camera") || assetManager.findImage("camera_image"))
}

export const RGB_MAX_VALUE = 256

export type ColorPaletteValue = {
  hex: string
  r?: number
  g?: number
  b?: number
  a?: number
  h?: number
  s?: number
  v?: number
}

export const parseColor = (color: ColorPaletteValue) =>
  color
    ? `${color.r / RGB_MAX_VALUE} ${color.g / RGB_MAX_VALUE} ${color.b / RGB_MAX_VALUE} ${color.a}`
    : "0 0 0 0"

export const parseColorFromEvalJs = (color?: string) => {
  if (!color || color === "0 0 0 0") color = "0 0 0 1"

  const rgba = color.split(" ").map((val) => parseFloat(val))
  rgba[0] *= RGB_MAX_VALUE
  rgba[1] *= RGB_MAX_VALUE
  rgba[2] *= RGB_MAX_VALUE

  return {
    hex: "#" + rgbHex(rgba[0], rgba[1], rgba[2], rgba[3]),
    r: rgba[0],
    g: rgba[1],
    b: rgba[2],
    a: rgba[3],
  }
}

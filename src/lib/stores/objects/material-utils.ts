import type { BanubaSDK } from "@banuba/webar"
import { nanoid } from "nanoid"
import { write, writeDir } from "../utils/fs"
import { findImage, findMaterialSampler } from "../utils/helpers"
import { vectorToArray } from "../objects/utils"

const aliases = {
  diffuse: ["tex_diffuse", "diffuse", "base_color"],
  metallic: ["tex_metallic", "metallic"],
  roughness: ["tex_roughness", "roughness"],
  normal: ["tex_normal", "normal"],
  iblDiff: ["ibl_diff.ktx", "ibl_diff"],
  iblSpec: ["ibl_spec.ktx", "ibl_spec"],
}

export const setTexture = async (
  effect: BanubaSDK.Effect,
  entity: BanubaSDK.Entity,
  sdk: typeof BanubaSDK,
  file: File,
  type: keyof typeof aliases
) => {
  const scene = effect.scene()
  const assetManager = scene.getAssetManager()
  const root = scene.getRoot()
  const newEntity = root.findChildByName(entity.getName())
  const meshInstance = newEntity.getComponent(sdk.ComponentType.MESH_INSTANCE).asMeshInstance()

  const subGeometryName = meshInstance.getMesh().getSubGeometries().get(0)
  const subGeometryMaterial = meshInstance.getSubGeometryMaterial(subGeometryName)

  if (!subGeometryMaterial) return

  const cwd = `${effect.url()}/images`
  const dist = `${cwd}/${file.name}`
  write(sdk["FS"], dist, new Uint8Array(await file.arrayBuffer()))

  const sample = findMaterialSampler(subGeometryMaterial, (sample) =>
    aliases[type].includes(sample)
  )

  const image = assetManager.createImage(file.name, sdk.ImageType.TEXTURE)
  subGeometryMaterial.addImage(sample || type, image)
  image.asTexture().load(dist)
}

export const setLightning = (
  effect: BanubaSDK.Effect,
  fs: typeof FS,
  files: Record<string, Uint8Array>
) => {
  const scene = effect.scene()
  const assetManager = scene.getAssetManager()

  const iblDiff = findImage(assetManager, aliases["iblDiff"])?.asCubemap()
  const iblSpec = findImage(assetManager, aliases["iblSpec"])?.asCubemap()

  const cwd = `${effect.url()}/${nanoid()}`

  const paths = Object.keys(files).map((name) => `${cwd}/${name}`)

  const iblDiffPath = paths.find((path) => path.includes("ibl_diff"))
  const iblSpecPath = paths.find((path) => path.includes("ibl_spec"))

  writeDir(fs, cwd, files)

  iblDiff?.load(iblDiffPath)
  iblSpec?.load(iblSpecPath)
}

export const findMaterialName = (
  type: keyof typeof aliases,
  materials: BanubaSDK.Material[]
): string | null => {
  if (!materials.length) return

  const material = materials[0]
  if (!material) return ""

  const samplers = vectorToArray(material.getSamplers())
  const images = vectorToArray(material.getImages())

  const sample = findMaterialSampler(material, (s) => aliases[type].includes(s))
  return images[samplers.findIndex((val) => val === sample)]?.getName() || ""
}

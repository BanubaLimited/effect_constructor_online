import type { Player } from "@banuba/webar"

export class RenderTarget {
  private readonly _player: Player
  private _lastRenderTarget

  constructor(player: Player) {
    this._player = player
    this._getLastRenderTarget()
  }

  getMSAAValue() {
    return this._lastRenderTarget?.getSamplesCount()
  }

  setMSAAValue(value: number) {
    if (value > 4) throw new Error("Amount of samples must be less or equal of possible by target")
    this._lastRenderTarget?.setSamplesCount(value)
  }

  private _getLastRenderTarget() {
    const list = this._player["_effectManager"].current().scene().getRenderList()
    let i = 0

    /**
     * can't use list.getTasksCount() for better cycle because of UnboundTypeError
     * https://github.com/emscripten-core/emscripten/issues/612#issuecomment-18428394
     */
    while (true) {
      try {
        this._lastRenderTarget = list.getTaskTarget(i++)
      } catch (e) {
        break
      }
    }
  }
}

export const vectorToArray = <T = any>(vector: Emscripten.Vector<T>): T[] => {
  const array = new Array<T>(vector.size())

  for (let i = 0; i < array.length; ++i) {
    array[i] = vector.get(i)
  }

  return array
}

export const radians = (degrees: number): number => degrees * (Math.PI / 180)
export const degrees = (radians: number): number => Math.round(radians * (180 / Math.PI))

export const codeEditorPlaceholder = "// Your code starts here\n\n"
export const codeEditorPlaceholderNoBreaks = "// Your code starts here"

import { derived } from "svelte/store"
import studio from "./studio"
import { makeup, effect } from "./effect"
import { RenderTarget } from "./objects/render-target"

export default derived<[typeof studio, typeof effect, typeof makeup], RenderTarget>(
  [studio, effect, makeup],
  ([{ player }, _, $makeup], set) => {
    if ($makeup) set(new RenderTarget(player))
  }
)

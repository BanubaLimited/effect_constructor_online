import { browser } from "$app/env"
import { get, derived, writable } from "svelte/store"

import { Player, Module } from "@banuba/webar"
import wasm from "@banuba/webar/BanubaSDK.wasm?url"
import simd from "@banuba/webar/BanubaSDK.simd.wasm?url"
import data from "@banuba/webar/BanubaSDK.data?url"
import FaceTracker from "@banuba/webar/face_tracker.zip?url"
import Background from "@banuba/webar/background.zip?url"
import Lips from "@banuba/webar/lips.zip?url"
import Hair from "@banuba/webar/hair.zip?url"
import Eyes from "@banuba/webar/eyes.zip?url"

import { isMobileDevice } from "$lib/utils"

const loading = writable({
  loaded: 0,
  total: Infinity,
  complete: false,
})

const fpsCounter = {
  cam: 0,
  processing: 0,
  render: 0,
}

export const fps = writable({
  cam: 0,
  processing: 0,
  render: 0,
})

const playerConfig = {
  clientToken: import.meta.env.VITE_BANUBA_CLIENT_TOKEN as string,

  locateFile: {
    "BanubaSDK.wasm": wasm,
    "BanubaSDK.simd.wasm": simd,
    "BanubaSDK.data": data,
  },

  // @ts-ignore: TODO: move the `onProgress` logic inside the BanubaSDK package for convenience
  setStatus: (msg: string) => {
    const [, loaded, total] = msg.match(/\((.*)\/(.*)\)/) ?? []

    if (total == null && loaded == null) return

    loading.update((state) => ({
      ...state,
      total: +total,
      loaded: +loaded,
    }))
  },
}

const player = writable<Player>(null, (set) => {
  if (!browser || isMobileDevice()) return

  Player.create({
    ...playerConfig,
    logger: {
      // debug: console.debug,
      // info: console.info,
      // warn: console.warn,
      error(detail: string) {
        get(player)?.dispatchEvent(new CustomEvent("log", { detail }))
      },
    },
    devicePixelRatio: 1,
  })
    .then(async (player) => {
      await player.addModule(
        ...[FaceTracker, Background, Lips, Eyes, Hair].map((m) => new Module(m))
      )

      player.addEventListener("framereceived", () => fpsCounter.cam++)
      player.addEventListener("frameprocessed", () => fpsCounter.processing++)
      player.addEventListener("framerendered", () => fpsCounter.render++)
      setInterval(() => {
        fps.set({ ...fpsCounter })
        fpsCounter.cam = 0
        fpsCounter.processing = 0
        fpsCounter.render = 0
      }, 1000)
      set(player)
    })
    .then((_) =>
      loading.update((state) => ({
        ...state,
        complete: true,
      }))
    )
})

export default derived([loading, player], ([$loading, $player]) => ({
  loading: $loading,
  player: $player,
  sdk: $player && $player["_sdk"],
}))

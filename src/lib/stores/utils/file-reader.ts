import uzip from "uzip"
import filesaver from "file-saver"

export const zip = (files: Record<string, Uint8Array>) => {
  const { encode: zip } = uzip
  const archive = zip(files)
  return new Blob([archive], { type: "application/zip" })
}

export const unzip = async (archive: File): Promise<Record<string, Uint8Array>> => {
  const buffer = await archive.arrayBuffer()
  const files = uzip.parse(buffer)
  const filtered = Object.entries(files)
    .filter(([name, val]) => !name.includes("__MACOSX"))
    .filter(([_, data]) => data.length)

  return Object.fromEntries(filtered) as Record<string, Uint8Array>
}

export const unzipByUrl = async (url: string): Promise<Record<string, Uint8Array>> => {
  const res = await fetch(url)
  const buffer = await res.arrayBuffer()
  return uzip.parse(buffer)
}

export const download = (files: Record<string, Uint8Array>, name: string) => {
  const { saveAs: save } = filesaver
  const archive = zip(files)
  save(archive, name)
}

export const downloadByUrl = async (url: string, name: string) => {
  const res = await fetch(url)
  filesaver.saveAs(await res.blob(), name)
}

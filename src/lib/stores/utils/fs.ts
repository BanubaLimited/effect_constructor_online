export const exists = (fs: typeof FS, path: string) => {
  try {
    fs.lstat(path)
    return true
  } catch (e) {
    if (e.errno === 44 || e.code === "ENOENT") return false
    else throw e
  }
}

export const readDirRecursive = (fs: typeof FS, dir: string, { includeDirs = false } = {}) => {
  const files: Record<string, Uint8Array> = {}
  const entries = fs.readdir(dir)

  entries.forEach((entry) => {
    const path = `${dir}/${entry}`
    const stats = fs.lstat(path)

    const isReserved = [".", ".."].includes(entry)
    const isFile = fs.isFile(stats.mode)
    const isDir = fs.isDir(stats.mode)

    if (isReserved) return

    if (isFile) {
      files[path] = fs.readFile(path)
      return
    }

    if (isDir) {
      const nested = readDirRecursive(fs, path)

      Object.assign(files, nested)

      if (includeDirs) Object.assign(files, { [path]: [] })
    }
  })

  return files
}

export const write = (fs: typeof FS, path: string, data: Uint8Array) => {
  const parts = path.split("/")
  if (parts[0] === "") parts.shift()

  if (parts.length > 1) {
    parts.reduce((full, part) => {
      if (!exists(fs, full)) fs.mkdir(full)

      return `${full}/${part}`
    })
  }

  fs.writeFile(path, data)
}

export const writeDir = (fs: typeof FS, root: string, files: Record<string, Uint8Array>) => {
  Object.entries(files).forEach(([name, value]) => {
    write(fs, `${root}/${name}`, value)
  })
}

export const rmrf = (fs: typeof FS, ...paths: string[]) => {
  if (paths.length > 1) {
    return paths.forEach((p) => rmrf(fs, p))
  }

  const [path] = paths
  if (!exists(fs, path)) return

  const stats = fs.lstat(path)
  const isFile = fs.isFile(stats.mode)
  const isDir = fs.isDir(stats.mode)

  if (isFile) {
    fs.unlink(path)
    return
  }

  if (isDir) {
    Object.keys(readDirRecursive(fs, path, { includeDirs: true })).forEach((childpath) =>
      rmrf(fs, childpath)
    )

    fs.rmdir(path)
  }
}

export const differenceDump = (prevPath: string, newPath: string, fs: typeof FS) => {
  const prevFiles = readDirRecursive(fs, prevPath)
  const newFiles = readDirRecursive(fs, newPath)

  const paths = Object.entries(newFiles).map(([idx, _]) => idx.replace(newPath, ""))

  Object.entries(prevFiles).forEach(([key, value]) => {
    const clearPath = key.replace(prevPath, "")
    if (!paths.includes(clearPath)) {
      write(fs, newPath + clearPath, value)
    }
  })
}

export const addScriptField = (fs: typeof FS, path: string) => {
  const configJsonPath = `${path}/config.json`
  const configJson = JSON.parse(new TextDecoder().decode(fs.readFile(configJsonPath)))

  configJson["script"] = {
    entry_point: "config.js",
    type: "latest",
  }

  fs.writeFile(configJsonPath, JSON.stringify(configJson, null, 2))
}

export const writeFile = async (fs: typeof FS, path: string, file: File) => {
  const uint8ArrayFile = new Uint8Array(await file.arrayBuffer())
  write(fs, path, uint8ArrayFile)
}

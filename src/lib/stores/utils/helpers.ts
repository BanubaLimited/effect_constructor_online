import { vectorToArray } from "../objects/utils"

export const findImage = (assetManager: BanubaSDK.AssetManager, names: string[]) => {
  return names.reduce((prev, cur) => prev || assetManager.findImage(cur), null)
}

export const findMaterialSampler = (
  material: BanubaSDK.Material,
  callback: (samplerName: string) => boolean
): string => {
  const samplers = vectorToArray(material.getSamplers())
  return samplers.find((sample) => callback(sample))
}

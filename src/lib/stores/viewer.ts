import { derived, writable } from "svelte/store"
import { Webcam, Image, Video, Dom } from "@banuba/webar"
import studio from "./studio"

export const DEFAULT_IMAGE_PATH_FEMALE = "/images/female.png"
export const DEFAULT_IMAGE_PATH_MALE = "/images/male.png"

export const DEFAULT_VIDEO_PATH_FEMALE = "/videos/girl-loop-HD.mp4"
export const DEFAULT_VIDEO_PATH_MALE = "/videos/male-loop-HD.mp4"

export type MediaSource = Webcam | Image | Video

export const source = writable<MediaSource>(null)
const container = writable<HTMLElement | string>(null)

export default derived([studio, source, container], ([{ player }, $source, $container]) => ({
  webcam: async () => {
    player.use(($source = await new Webcam().start()))
  },

  photo: (src?: string | File | Blob) => {
    player.use(($source = new Image((src ?? DEFAULT_IMAGE_PATH_MALE) as string)))
    player.play({ pauseOnEmpty: false })
  },

  video: (src?: string | File | Blob) => {
    player.use(($source = new Video((src ?? DEFAULT_VIDEO_PATH_MALE) as string, { loop: true })))
  },

  render: (container: HTMLElement | string) => {
    Dom.render(player, ($container = container))
  },

  unmount: () => {
    if ($source) {
      if ("stop" in $source) $source.stop()
      source.set(null)
    }

    if ($container) {
      Dom.unmount($container)
    }

    container.set(null)
  },
}))

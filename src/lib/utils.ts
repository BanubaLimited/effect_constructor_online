import { browser } from "$app/env"

export const promptUploadFile = ({ accept = "*", isTexture = false } = {}) => {
  const input = document.createElement("input")
  input.type = "file"
  input.accept = accept
  input.style.position = "absolute"
  input.style.zIndex = "-99999"
  input.style["appearance"] = "none"

  return new Promise<File>((resolve) => {
    // Safari requires the input to be in DOM
    document.body.appendChild(input)

    input.onchange = ({ target }) => {
      document.body.removeChild(input)
      const file = (target as typeof input).files[0]
      // FIXME: sdk resource manager does not work with special symbols
      resolve(isTexture ? new File([file], removeSpecialSymbols(file.name)) : file)
    }

    input.click()
  })
}

export const isMobileDevice = (): boolean =>
  browser
    ? /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    : false

export const countDecimals = (value: number): number => {
  if (Math.floor(value) === value) {
    return 0
  }

  return value.toString().split(".")[1].length || 0
}

export const removeSpecialSymbols = (str: string) => str.replace(/[^a-zA-Z0-9.]/g, "_")

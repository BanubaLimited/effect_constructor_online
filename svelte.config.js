import sveltePreprocess from "svelte-preprocess"
import adapter from "@sveltejs/adapter-static"

const config = {
  preprocess: sveltePreprocess({
    postcss: true,
  }),
  kit: {
    adapter: adapter({
      fallback: "index.html",
    }),
    trailingSlash: "always",
  },
  onwarn: (warning, handler) => {
    if (warning.code.startsWith("a11y-")) return
    if (warning.code === "css-unused-selector" && warning.message.includes(".focus-visible")) return

    handler(warning)
  },
}

export default config

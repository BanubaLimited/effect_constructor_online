from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
import urllib.parse
import sys
import os
from datetime import datetime
import json



path = sys.argv[1] # path to folder with files


def get_max_date(file_name):
    dates_list = list()

    list_files = os.listdir(path)

    for file in list_files:
        if file.startswith(file_name) and file.endswith(".zip"):
            file_name_items = os.path.splitext(file)
            items = file_name_items[0].split("_")

            res = True
            try:
                res = bool(datetime.strptime(items[-1], "%Y-%m-%d"))
                dates_list.append(items[-1])
            except:
                res = False
    
    return max(dates_list) 


def get_file(file_name):
    last_date = get_max_date(file_name)

    with open(f"{path}/{file_name}_{last_date}.zip", "rb") as file:
        data_bytes = file.read()

    return data_bytes, last_date


class HttpGetHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        try:
            # http://127.0.0.1:8800/get_date?file_name=file_one - get the latest date of file with "file_one" prefix
            if self.path.startswith("/get_date"):
                params_str = self.path[len("/get_date?"):]
                params = urllib.parse.parse_qs(params_str)
                file_name = params['file_name'][0]

                last_date = get_max_date(file_name)
                body = json.dumps({"date": last_date})

                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.send_header("Content-Length", f"{len(body)}")
                self.end_headers()
                self.wfile.write(body.encode(encoding='utf_8'))

            # http://127.0.0.1:8800/get_file?file_name=file_one - get the latest file with "file_one" prefix
            elif self.path.startswith(("/get_file")):
                params_str = self.path[len("/get_file?"):]
                params = urllib.parse.parse_qs(params_str)
                file_name = params['file_name'][0]

                last_file, last_date = get_file(file_name)

                self.send_response(200)
                self.send_header("Content-type", "application/zip")
                self.send_header("Content-Disposition", f"attachment; filename=\"{file_name}_{last_date}.zip\"")
                self.send_header("Content-Length", f"{len(last_file)}")
                self.end_headers()
                self.wfile.write(last_file)
            else:
                body = json.dumps({"result": "not found"})

                self.send_response(403)
                self.send_header("Content-type", "application/json")
                self.send_header("Content-Length", f"{len(body)}")
                self.end_headers()
                self.wfile.write(body.encode(encoding='utf_8'))
        except Exception as e:
            print(e)
            self.send_response(503)


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
  server_address = ('', 8800)
  httpd = server_class(server_address, handler_class)
  try:
      httpd.serve_forever()
  except KeyboardInterrupt:
      httpd.server_close()


    
if __name__ == '__main__': 
    # Run process:
    run(handler_class=HttpGetHandler)




# Browser: 
# 127.0.0.1:8800

# API: 
# http://127.0.0.1:8800/get_date?file_name=file_name - get the latest date of file with "file_name" prefix
# http://127.0.0.1:8800/get_file?file_name=file_name - get the latest file with "file_name" prefix

# File name format:
# <file_name>_<date>.zip

# Command: 
# python3 /update_server/web_server.py /update_server/files
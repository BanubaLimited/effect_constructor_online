import path from "path"
import { sveltekit } from "@sveltejs/kit/vite"

export default {
  mode: process.env.APP_ENV,
  build: {
    target: "es2018",
  },
  resolve: {
    alias: {
      static: path.resolve("static"),
    },
  },
  plugins: [sveltekit()],
  server: {
    hmr: {
      overlay: false,
    },
    fs: {
      strict: false,
      allow: [".."],
    },
  },
}
